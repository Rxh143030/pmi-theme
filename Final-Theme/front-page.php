<?php get_header(); ?>

	<main id="main_sec">
        <div>         
            <div class="container mt-4 mb-4">
                <div class="row mb-4">
                    <div class="col-md-6 mb-4">
                        <img src="Images/JobCost.jpg" class="img-fluid" alt="Responsive image">
                        <h4 class="MFeat_Cat_Bar">Property Management</h4>
                        <h1 class="MFeat_Title" style="margin-bottom: .3em"><a href="#">How Complete is Your Property Management Accounting Software? [eBook]</a></h1>
					  	<p class="MFeat_Author mt-2">By <a href="#">Guy Lyman</a> <font color="F55E24"> | Sep 20, 2017</font></p>

                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
								<div class="sub_Container">
									<p class="sub_Foot_Title_Sec"><font style="color: #f06000">Subscribe</font> to PMI</p>
									<p class="sub_P">Sign up for our weekly newsletter to receive a summary of the week on Property Management Insider</p>
									<hr style="background: #3c4349; height:1px">
									
									<div class="sub_Foot_Group my-2">
										<input class="sub_Foot_Input" type="text" id="fname1" required="required"/>
										<label class="sub_Foot_Label" for="fname1">First Name</label>
										<div class="sub_bar"></div>
									</div>

									<div class="sub_Foot_Group my-2">
										<input class="sub_Foot_Input" type="text" id="lname1" required="required"/>
										<label class="sub_Foot_Label" for="lname1">Last Name</label>
										<div class="sub_bar"></div>
									</div>

									<div class="sub_Foot_Group my-2">
										<input class="sub_Foot_Input" type="text" id="email1" required="required"/>
										<label class="sub_Foot_Label" for="email1">Email</label>
										<div class="sub_bar"></div>
									</div>
								
									 <button class="sub_Foot_Button my-2 ">Submit</button>
									
								</div>
							</div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 pb-3 main_Item">
                                <div class="MFeat_img_container">
                                    <img src="Images/JobCost.jpg" class="img-fluid" alt="Responsive image">
                                </div>
                                <p class="MFeat_Cat"><a href="#">Property Management</a></p>
                                <h3 class="MFeat_Title2">How Complete is Your Property Management Accounting Software? [eBook]</h3>
                               	<p class="MFeat_Author mt-2">By <a href="#">Guy Lyman</a> <font color="F55E24"> | Sep 20, 2017</font></p>
                            </div>
                            <div class="col-md-6 pb-3">
                                <div class="MFeat_img_container">
                                    <img src="Images/JobCost.jpg" class="img-fluid" alt="Responsive image">
                                </div>
                                <p class="MFeat_Cat"><a href="#">Property Management</a></p>
                                <h3 class="MFeat_Title2">How Complete is Your Property Management Accounting Software? [eBook]</h3>
								<p class="MFeat_Author mt-2">By <a href="#">Guy Lyman</a> <font color="F55E24"> | Sep 20, 2017</font></p>
                            </div>
						</div>
					</div>
				</div>
				
				<hr>		
			</div>		
		</div>
		
		<!--		/MAIN-CONTENT(LATEST ARTICLES / POPULAR			-->
		<div class="container mb-4">

			  <div class="row">
				<div id="Latest_Content" class="col-md-8 article_Container">
					<div class="row">
						<div class="container">
							<h3 class="pop_Title response-center" style="width: 187.938px;">Latest Articles</h3>
						</div>
					</div>
				  	<div class="row"> 
					<?php

					   	$args = array('posts_per_page' => 6);
					   	$latest_posts = new WP_Query($args);

					   	if($latest_posts->have_posts()) : 
						  	while($latest_posts->have_posts()) : 
							 	$latest_posts->the_post();
								
					?>
						<div class="col-md-6 my-3 art_Item">
							<div class="MFeat_img_container ">
								<img src="<?php get_the_post_thumbnail(get_the_ID()); ?>" class="img-fluid" alt="Image">
							</div>
							<p class="MFeat_Cat"><a href=#><?php the_category(); ?></a></p>
							<h3 class="MFeat_Title2"><?php the_title() ?></h3>
							<p class="MFeat_Author mt-2">By <a href="#"><?php the_author(); ?></a> <font color="F55E24"> | <?php the_date('M j, Y'); ?></font></p>	
						</div>
						
					<?php
						endwhile;
						wp_reset_postdata();
					   	endif;
					?>
						
				  	</div>
					
				</div>
				  
				<div id="Popular" class="col-md-4">
					<div class="container">
						<div class="row">					
								<h3 class="pop_Title response-center" style="width: 175.969px;">Most Popular</h3>
						</div>
						<div  class="row py-3 pop_Row">
							<div style="float: left; max-width: 40%;">
								<img src="Images/JobCost.jpg" class="img-fluid" alt="Responsive image">
							</div>
							<div class="pop_Item">
								<h3 class="MFeat_Title2">How Complete is Your Property Management Accounting Software? [eBook]</h3>
							</div>				
							<p class="MFeat_Author mt-2">By <a href="#">Guy Lyman</a> <font color="F55E24"> | Sep 20, 2017</font></p>
						</div>
						<div class="row py-3 pop_Row">
							<div style="float: left; max-width: 40%;">
								<img src="Images/JobCost.jpg" class="img-fluid" alt="Responsive image">
							</div>
							<div class="pop_Item">
								<h3 class="MFeat_Title2">How Complete is Your Property Management Accounting Software? [eBook]</h3>

							</div>				
							<p class="MFeat_Author mt-2">By <a href="#">Guy Lyman</a> <font color="F55E24"> | Sep 20, 2017</font></p>
						</div>
						<div class="row py-3 pop_Row" >
							<div style="float: left; max-width: 40%;">
								<img src="Images/JobCost.jpg" class="img-fluid" alt="Responsive image">
							</div>
							<div class="pop_Item">
								<h3 class="MFeat_Title2">How Complete is Your Property Management Accounting Software? [eBook]</h3>		
							</div>				
							<p class="MFeat_Author mt-2">By <a href="#">Guy Lyman</a> <font color="F55E24"> | Sep 20, 2017</font></p>
						</div>
					</div> 
				</div> 
				
				<hr>
				  
			</div> 
		</div> 
	</main>

<?php get_footer(); ?>