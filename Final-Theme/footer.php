<footer>
		
		<!--	SUBSCRIBE FOOTER	-->
		
		<div id="Sub_Footer" class="container my-5 sub_foot_Container">
			<div style="border: 1px solid #f0f0f0; margin-bottom: 3em;"></div>
			<div class="row justify-content-around">
				<div id="subFootCall" class="col-md-5">
					<p class="sub_Foot_Par">Sign up for our weekly newsletter to receive a summary of the week on Property Management Insider</p>
				</div>
				<div class="col-md-5">
					<p class="sub_Foot_Title_Sec">Don't Forget to <font style="color: #f06000">Subscribe</font></p>
					<hr style="height: 2px; background-color: #f06000">
					
					<div class="sub_Foot_Group my-4">
						<input class="sub_Foot_Input" type="text" id="fname2" required="required"/>
						<label class="sub_Foot_Label" for="fname2">First Name</label>
						<div class="sub_bar"></div>
					</div>
					
					<div class="sub_Foot_Group my-4">
						<input class="sub_Foot_Input" type="text" id="lname2" required="required"/>
						<label class="sub_Foot_Label" for="lname2">Last Name</label>
						<div class="sub_bar"></div>
					</div>
					
					<div class="sub_Foot_Group my-4">
						<input class="sub_Foot_Input" type="text" id="email2" required="required"/>
						<label class="sub_Foot_Label" for="email2">Email</label>
						<div class="sub_bar"></div>
					</div>
					 <button class="sub_Foot_Button my-2">Submit</button>
				</div>
			</div>
		</div>
		
		<div >
		
		</div>
		
		<div class="foot_Copyright">
			<div class="container">
				© RealPage, Inc. All trademarks are the properties of their respective owners. 1-877-325-7243 |
				<a href="https://www.propertymanagementinsider.com/terms-conditions" style="color: #f06000">Terms & Conditions</a>
				|
				<a href="https://www.propertymanagementinsider.com/privacy-policy" style="color: #f06000">Privacy Policy</a>
				|
				<a href="https://www.realpage.com/dmca-notice/" style="color: #f06000">DMCA Notice</a>
			</div>
		</div>
    </footer>
   
	<?php wp_footer(); ?>
	
</body>    


</html>