<?php
	
//	Enqueue Styles
function wpt_theme_styles(){
	
	wp_enqueue_style( 'style_css', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'bootstrap_css', get_template_directory_uri() . '/css/bootstrap.css' );
	wp_enqueue_style( 'work_sans', 'https://fonts.googleapis.com/css?family=Work+Sans:400,800' );
	wp_enqueue_style( 'roboto', 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700');
	wp_enqueue_style( 'quicksand', 'https://fonts.googleapis.com/css?family=Quicksand');
}

add_action( 'wp_enqueue_scripts', 'wpt_theme_styles');

function wpt_theme_scripts(){
	
	wp_enqueue_script( 'scripts_js', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '', true);
	wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() . '/js/bootstrap.js', array('jquery'), '', true);
}

add_action( 'wp_enqueue_scripts', 'wpt_theme_scripts');

?>