<!DOCTYPE HTML>
<html>

<head>
    <title><?php wp_title(); ?></title>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<?php wp_head(); ?>
  
</head>
	
<body>
	
	<!--	HEADER/NAVAGATION	-->
    <header>  
        <div id="Header" class="container-fluid ml-4">
            <img src="http://pmi.site/wp-content/uploads/2017/11/PMI_Logo.png" class="img-fluid LOGO" alt="Responsive image">
        </div>
			<div id="Navbar" class="Nav_Container clearfix">
				
				<div class="rel_container">
				<div class="container-fluid nav_ham ml-2">
					<div class="hamburger_icon">
						<span></span>
						<span></span>
						<span></span>
					</div>
					
					<div class="sidebar">
						<ul class ="sidebar_menu">
							<li><a href="#">Property Management</a></li>
							<li><a href="#">MPF Research</a></li>
							<li><a href="#">Revenue Management</a></li>
							<li><a href="#">Expense Management</a></li>
							<li><a href="#">Apartment Marketing</a></li>
						</ul>
					</div>
				</div>
				</div>
				

				<div class="container-fluid ml-4 menu-hover-lines">
					<ul class="Nav_Bar">
						<li><a href="#">Property Management</a></li>
						<li><a href="#">MPF Research</a></li>
						<li><a href="#">Revenue Management</a></li>
						<li><a href="#">Expense Management</a></li>
						<li><a href="#">Apartment Marketing</a></li>
					</ul>
				</div>			
			</div>
			<div class="navfix"></div>
		
    </header>